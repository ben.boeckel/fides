Running Gray-scott example with SST:

First in `adios2-fides.xml`, change the engine type for `SimulationOutput` to SST.
```xml
<io name="SimulationOutput">
  <engine type="SST">
  ...
  </engine
</io>
```

Then to run the simulation:
```
fides-build/examples/gray-scott$ mpirun -np 4 ./gray-scott settings-staging.json
```

To run the reader:
```
fides-build/examples/gray-scott$ ./fides-sst-reader
```
