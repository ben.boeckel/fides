//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include <fstream>
#include <ios>
#include <iostream>
#include <string>
#include <vector>

#include "SchemaValidator.h"

int main(int argc, char* argv[])
{
  if (argc != 3)
  {
    std::cerr << "Usage: " << std::string(argv[0]) << " schema.json input.json" << std::endl;
    return EXIT_FAILURE;
  }

  SchemaValidator validator((std::string(argv[1])));
  if (!validator.SetJSONFile(std::string(argv[2])))
  {
    std::cerr << "Opening " << std::string(argv[2]) << " failed" << std::endl;
    return EXIT_FAILURE;
  }

  if (!validator.Validate())
  {
    std::cerr << "Validation failed" << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
