set(srcs
  Array.cxx
  CellSet.cxx
  CoordinateSystem.cxx
  DataModel.cxx
  DataSetReader.cxx
  DataSetWriter.cxx
  DataSource.cxx
  FidesTypes.cxx
  Field.cxx
  FieldData.cxx
  FieldDataManager.cxx
  Keys.cxx
  MetaData.cxx
  Value.cxx
)

set(headers
  Array.h
  CellSet.h
  Configure.h
  CoordinateSystem.h
  DataModel.h
  DataSetReader.h
  DataSetWriter.h
  DataSource.h
  Deprecated.h
  FidesTypes.h
  Field.h
  FieldData.h
  FieldDataManager.h
  Keys.h
  MetaData.h
  Value.h
)

include(${FIDES_SOURCE_DIR}/cmake/fides_generate_export_header.cmake)
add_library(fides ${srcs})

fides_generate_export_header(fides)

target_compile_definitions(fides PRIVATE ${MPI_DEFS})
target_link_libraries(fides PUBLIC vtkm_cont adios2::adios2 PRIVATE
  fides_rapidjson)

target_include_directories(fides PUBLIC
  $<BUILD_INTERFACE:${FIDES_SOURCE_DIR}>
  $<BUILD_INTERFACE:${FIDES_BINARY_DIR}/fides>
  $<INSTALL_INTERFACE:include>
  )

add_subdirectory(xgc)
add_subdirectory(predefined)

#-------------------------------------------------------------------------------
# generate the config file that includes the exports
include(CMakePackageConfigHelpers)
configure_package_config_file(${FIDES_SOURCE_DIR}/cmake/Config.cmake.in
  "${FIDES_BINARY_DIR}/FidesConfig.cmake"
  INSTALL_DESTINATION lib/cmake/fides
  NO_SET_AND_CHECK_MACRO
  NO_CHECK_REQUIRED_COMPONENTS_MACRO
  )

install(FILES "${FIDES_BINARY_DIR}/FidesConfig.cmake" DESTINATION lib/cmake/fides)

install(TARGETS fides DESTINATION lib
  EXPORT ${FIDES_EXPORT_NAME})

install(EXPORT ${FIDES_EXPORT_NAME} FILE FidesTargets.cmake DESTINATION lib/cmake/fides)

export(EXPORT ${FIDES_EXPORT_NAME}
  FILE FidesTargets.cmake
  )

install(FILES ${headers} ${CMAKE_CURRENT_BINARY_DIR}/fides_export.h
  DESTINATION ${FIDES_INSTALL_INCLUDE_DIR})
